# springboot-learning-helloword
## Running springboot-learning helloword

1. Using the Maven plugin
```shell script
$ mvn spring-boot:run
```
2. Running As a Packaged application
```shell script
$ java -jar target/springboot-helloword-0.0.1-SNAPSHOT.jar
```
# 扫码关注本人公众号 或者搜索公众号： 
<img src="https://gitee.com/LongGroup/tinygray_picturebed/raw/master/wechat/wechat_8cm.jpg" width="300" title="公众号:Madison龙少" alt="">
