package com.tinygrey.springboothelloword.controller;

import cn.hutool.core.util.StrUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWordController {

    /**
     * Hello，World
     *
     * @param what 参数，非必须
     * @return "Hello,{what}!
     */
    @GetMapping("hello")
    public String sayHello(@RequestParam(required = false, name = "what") String what) {
        if (StrUtil.isBlank(what)) {
            what = "Word";
        }
        return StrUtil.format("Hello,{}!", what);
    }
}
