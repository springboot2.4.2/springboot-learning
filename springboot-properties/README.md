# springboot-learning-properties

> 本 demo 演示配置文件的加载顺序，以及如何获取配置文件的自定义配置和自定义配置文件的自定义配置，以及如何多环境下的配置文件信息的获取
## 
1. **配置文件的加载顺序**
`file:./config/application.yml > file:./application.yml > classpath:config/application.yml > classpath:/application.yml`
2. **获取配置文件的自定义配置 || 获取自定义配置文件的自定义配置**
```java
/**
 * @PropertySource(value = "classpath:custom-profile.properties")
 * 重要重要重要：：：：里面加载的配置文件必须是"xxxx.properties"类型的。yml、yaml等等其他文件我了解的暂时不能加载
 *
 * @ConfigurationProperties(prefix = "person")
 *      默认加载application.properties下面的person开头的属性
 *      当存在@PropertySource("classpath:config/custom-profile.properties") 时候 会加载@PropertySource 指定的配置文件下面的属性
 * 比如：
 * @PropertySource("classpath:config/custom-profile.application")
 * @ConfigurationProperties(prefix = "person")
 * 加载custom-profile.properties下面的person开头的属性
 *
 *
 * 一般情况下 为了application.properties配置文件简洁  该配置文件下面最好放springboot自动装配的属性
 * 其他的自定义的属性 重新写一个配置文件  这样更加清晰明了  不至于很乱
 * 当然了 也可以在application.yml配置文件里面写
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Component
@PropertySource(value = "classpath:config/custom-profile.properties", encoding = "utf-8")
@ConfigurationProperties(prefix = "person")
public class Person {
    @Value("${person.name}")
    private String name;
    @Value("${person.age}")
    private int age;
    @Value("${person.birthday}")
    private Date birthday;
    private List<String> hobby;
    private Map<String, Object> assets;
    private Dog dog;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Component
@PropertySource(value = "classpath:config/custom-profile.properties", encoding = "utf-8")
@ConfigurationProperties(prefix = "dog")
class Dog {
    private String name;
    private Integer age;
}
```
```yaml
server:
  port: 8081
  servlet:
    context-path: /springboot-properties
spring:
  profiles:
    active: dev
#person:
#  name: tinygrey
#  age: 18
#  birthday: 2002/04/02
#  hobby:
#    - 打篮球
#    - 睡觉
#    - 玩游戏
#    - 学习
#  assets:
#    phone: iphone 12
#    car: 捷豹
#  dog:
#    name: 奶狗
#    age: 3
```
## 多环境下的配置文件信息的获取
### 问题分析
为什么SpringBoot需要用到多环境配置呢？其实多环境，指的就是生产环境、开发环境、测试环境等相互切换，那么此时，如果我们直接修改配置文件，就会造成我们大量的无价值工作，极大的降低我们的工作效率，所以此时，我们就会需要更加高效的实现开发环境切换的方法。
### 思路分析
目前来说，我们实现配置切换，大致来说，无非有三种。<br/>
1.同一配置文件写不同的配置，然后选择要加载的配置
```yaml
# 主配置
spring:
  profiles:
    active: dev # 选择开发环境的配置
---
# 选配一
server:
  port: 8081 # 配置的测试环境的端口
spring:
  profiles: test # 测试环境配置的名称

---
# 选配二
server:
  port: 8082 # 配置的开发环境的端口
spring:
  profiles: dev # 开发环境配置的名称

---
# 选配二
server:
  port: 8083 # 配置的开发环境的端口
spring:
  profiles: prop # 生产环境配置的名称
```
2.写多个配置文件，在主配置文件里面选择配置哪一种配置
> 创建3个配置文件，主配置文件文件名为application.yml，测试环境配置文件名为application-test.yml，开发环境配置文件名为application-dev.yml,生产环境配置文件名为application-prop.yml。
  这里需要进行说明的是，为什么要这样命名，是因为SpringBoot底层就会根据配置文件名的：“-”后面的内容去匹配不同的配置文件，重点在于那个：“-”，所以为了命名规范，我写的名字是这样的。
- application.yml
```yaml
server:
  port: 8081
  servlet:
    context-path: /springboot-properties
#选择需要的配置文件
spring:
  profiles:
    active: dev
```
- application-test.yml
```yaml
spring:
  profiles:
    - test
```
- application-dev.yml
```yaml
spring:
  profiles:
    - dev
```
- application-prop.yml
```yaml
spring:
  profiles:
    - prop
```

3.项目上线之后，直接通过命令的方式，将配置修改为我们需要的配置类型（扩展吧，有的人会觉得只有前两个。每个人想法都不一样嘛。）
> 这个方式的前提肯定是我们配置了上面两种方式的其中一种的前提下哈
```shell script
java -jar projectName.jar --spring.profiles.active=dev
```
# 扫码关注本人公众号 或者搜索公众号： 
<img src="https://gitee.com/LongGroup/tinygray_picturebed/raw/master/wechat/wechat_8cm.jpg" width="300" title="公众号:Madison龙少" alt="">
