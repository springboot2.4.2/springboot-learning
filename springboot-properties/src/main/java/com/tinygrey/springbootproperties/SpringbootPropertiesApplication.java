package com.tinygrey.springbootproperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPropertiesApplication {

    public static void main(String[] args) {
        //启动类激活profiles
        //System.setProperty("spring.profiles.active", "test");
        SpringApplication.run(SpringbootPropertiesApplication.class, args);
    }

}
