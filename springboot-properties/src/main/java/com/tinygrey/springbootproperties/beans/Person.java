package com.tinygrey.springbootproperties.beans;

import com.tinygrey.springbootproperties.config.YmlPropertySourceFactory;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @PropertySource(value = "classpath:custom-profile.properties")
 * 重要重要重要：：：：里面加载的配置文件必须是"xxxx.properties"类型的。yml、yaml等等其他文件我了解的暂时不能加载
 * @ConfigurationProperties(prefix = "person")
 * 默认加载application.properties下面的person开头的属性
 * 当存在@PropertySource("classpath:config/custom-profile.properties") 时候 会加载@PropertySource 指定的配置文件下面的属性
 * 比如：
 * @PropertySource("classpath:config/custom-profile.application")
 * @ConfigurationProperties(prefix = "person")
 * 加载custom-profile.properties下面的person开头的属性
 * <p>
 * <p>
 * 一般情况下 为了application.properties配置文件简洁  该配置文件下面最好放springboot自动装配的属性
 * 其他的自定义的属性 重新写一个配置文件  这样更加清晰明了  不至于很乱
 * 当然了 也可以在application.yml配置文件里面写
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Component
@PropertySource(value = {"classpath:config/custom-profile.yml"}, encoding = "utf-8", factory = YmlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "person")
public class Person {
    @Value("${person.name}")
    private String name;
    @Value("${person.age}")
    private int age;
    @Value("${person.birthday}")
    private Date birthday;
    private List<String> hobby;
    private Map<String, Object> assets;
    private Dog dog;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Component
@PropertySource(value = "classpath:config/custom-profile.yml", encoding = "utf-8", factory = YmlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "dog")
class Dog {
    private String name;
    private Integer age;
}
