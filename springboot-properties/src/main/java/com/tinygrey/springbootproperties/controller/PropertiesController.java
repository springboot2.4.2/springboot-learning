package com.tinygrey.springbootproperties.controller;

import cn.hutool.core.lang.Dict;
import com.tinygrey.springbootproperties.beans.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("properties")
@Slf4j
public class PropertiesController {
    final
    Person person;

    public PropertiesController(Person person) {
        this.person = person;
    }
    @GetMapping("getProperties")
    public Dict getProperties(){
        log.info(person.toString());
        return Dict.create().set("person", person);
    }
}
