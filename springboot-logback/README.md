# springboot-logback

###### 为什么选择logback而不是log4j，请参考[【中文版】从Log4j迁移到LogBack的理由](https://www.oschina.net/translate/reasons-to-prefer-logbak-over-log4j)
###### [【英文版】从Log4j迁移到LogBack的理由](https://www.oschina.net/translate/reasons-to-prefer-logbak-over-log4j)

## 有限的生命 创造无限的价值
### 更多信息，请关注：
1. 公众号: 搜索 [ madison龙少 ]
2. [我的博客](http://www.tinygray.cn)
3. [我的微博](https://weibo.com/5350113092)


