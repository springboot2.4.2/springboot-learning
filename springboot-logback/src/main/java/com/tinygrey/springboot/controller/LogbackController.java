package com.tinygrey.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@PropertySource("classpath:logbackControllerProperties.properties")
@RestController
@RequestMapping("logback")
public class LogbackController {

    /**
     * spring官方文檔：https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-logging
     */
    private static final Logger logger = LoggerFactory.getLogger(LogbackController.class);
    @Value("${app.name}")
    String projectName;
    @GetMapping("logback")
    public String logback() {
        /*
            logback.xml中指定的默认输出级别为INFO，所以低于INFO级别(DEBUG、TRACE)的日志不会输出
            级别排序为： TRACE < DEBUG < INFO < WARN < ERROR
         */
        logger.info("{} -- This is a primary with logback., Current time {}.", projectName, new Date());
        logger.trace("This level is TRACE.");
        logger.debug("This level is DEBUG.");
        logger.debug("isDebugEnabled:" + logger.isDebugEnabled());
        logger.info("This level is INFO.");
        logger.info("isInfoEnabled:" + logger.isInfoEnabled());
        logger.warn("This level is WARN.");
        logger.error("This level is ERROR.");
        return "logback.";
    }
}
